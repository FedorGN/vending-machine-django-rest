from rest_framework import serializers
from app.vendingmachine.models import MyUser, Product


class RegistrationSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(
        style={"input_type": "password"}, write_only=True)

    class Meta:
        model = MyUser
        fields = ['username', 'password', 'password2', 'role']
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def save(self):
        user = MyUser(
            username=self.validated_data['username'], role=self.validated_data['role'])
        password = self.validated_data['password']
        password2 = self.validated_data['password2']
        if password != password2:
            raise serializers.ValidationError(
                {'password': 'Passwords must match.'})
        user.set_password(password)
        user.save()
        return user


class DepositValidator():
    """
    Validator for checking if that the deposit is multiple of 5.
    """
    message = ('Deposit should be multiple of 5.')

    def __init__(self, deposit="deposit", message=None):
        self.deposit = deposit
        self.message = message or self.message

    def __call__(self, attrs):
        if attrs[self.deposit] % 5 != 0:
            message = self.message
            raise serializers.ValidationError(message)


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = MyUser
        fields = ['username', 'deposit', 'is_admin', 'id']
        validators = [DepositValidator()]


class CostValidator():
    """
    Validator for checking if that the cost is greater than zero.
    """
    message = ('Coin must be multiple of 5')

    def __init__(self, cost="cost", message=None):
        self.cost = cost
        self.message = message or self.message

    def __call__(self, attrs):
        if attrs[self.cost] % 5 != 0:
            message = self.message
            raise serializers.ValidationError(message)


class AmountAvailableValidator():
    """
    Validator for checking if that the amountAvailable is greater or equal to zero.
    """
    message = ('amountAvailable should be greater or equal to zero.')

    def __init__(self, amountAvailable="amountAvailable", message=None):
        self.amountAvailable = amountAvailable
        self.message = message or self.message

    def __call__(self, attrs):
        if attrs[self.amountAvailable] < 0:
            message = self.message
            raise serializers.ValidationError(message)


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ['cost', 'amountAvailable', 'productName', 'seller']
        validators = [CostValidator(), AmountAvailableValidator()]
