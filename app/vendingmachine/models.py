from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)


class MyUserManager(BaseUserManager):
    def create_user(self, username, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not username:
            raise ValueError('Username must not be empty')

        user = self.model(username=username)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            username,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser):
    """Custom user class."""
    ROLES_CHOICES = [
        ('seller', 'Seller'),
        ('buyer', 'Buyer')
    ]

    username = models.CharField(
        max_length=255,
        unique=True,
    )
    is_admin = models.BooleanField(default=False)
    deposit = models.PositiveIntegerField(default=0)
    role = models.CharField(max_length=255, choices=ROLES_CHOICES)

    objects = MyUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['role']

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        return True

    @property
    def is_staff(self):
        return self.is_admin

    def reset_deposit(self) -> None:
        """Reset user deposit to zero."""
        self.deposit = 0
        return None

    def get_change(self, cost: int) -> list[int]:
        """
        Get change (list of coins).
        """
        coins: list[int] = [100, 50, 20, 10, 5]
        change_total: int = self.deposit - cost
        change: list[int] = []
        coins_number: list[int] = []
        for coin in coins:
            coins_number.append(change_total//coin)
            change_total %= coin
        change: list[int] = []
        for i in range(len(coins_number)):
            if coins_number[i]:
                change += [coins[i]]*coins_number[i]
        return change


class Product(models.Model):
    """Product information class."""
    cost: int = models.PositiveIntegerField(null=False)
    amountAvailable: int = models.PositiveIntegerField(null=False, default=0)
    productName: str = models.CharField(max_length=200, null=False)
    seller = models.ForeignKey(
        MyUser, related_name='products', on_delete=models.CASCADE)

    def is_available(self):
        """Check if the product is available."""
        if self.amountAvailable > 0:
            return True
        return False
