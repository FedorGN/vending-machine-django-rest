from django.urls import path

from app.vendingmachine.views import (HelloView, RegistrationView, LoginView, LogoutView,
                                      CRUDProductView, DepositView, ResetDepositView,
                                      BuyProductView)
from rest_framework_simplejwt import views as jwt_views


urlpatterns = [
    path('hello/', HelloView.as_view(), name='hello'),
    path('accounts/user', RegistrationView.as_view(), name='register'),
    path('accounts/login', LoginView.as_view(), name='register'),
    path('accounts/logout', LogoutView.as_view(), name='register'),
    path('accounts/token-refresh/',
         jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('product/', CRUDProductView.as_view(), name='product'),
    path('deposit/', DepositView.as_view(), name='deposit'),
    path('reset/', ResetDepositView.as_view(), name='reset'),
    path('buy/', BuyProductView.as_view(), name='buy_product')

]
