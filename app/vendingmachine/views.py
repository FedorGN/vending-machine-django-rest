from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from rest_framework import status

from app.vendingmachine.models import Product, MyUser
from app.vendingmachine.permissions import SellerOnly, BuyerOnly
from app.vendingmachine.utils import get_tokens_for_user
from app.vendingmachine.serializers import RegistrationSerializer, ProductSerializer, UserSerializer


class RegistrationView(APIView):
    """
    post:
    Create a new user.
    """

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'username': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
            'password': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
            'password2': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
            'role': openapi.Schema(type=openapi.TYPE_STRING, description='seller'),
        }
    ))
    def post(self, request):
        serializer = RegistrationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LoginView(APIView):
    """
    post:
    User log in with a username and password.
    """

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'username': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
            'password': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
        }
    ))
    def post(self, request):
        username: str = request.data.get('username')
        password: str = request.data.get('password')
        if not username or not password:
            return Response({'msg': 'Credentials missing'}, status=status.HTTP_400_BAD_REQUEST)
        user: MyUser = authenticate(
            request, username=username, password=password)
        if user is not None:
            login(request, user)
            auth_data = get_tokens_for_user(request.user)
            return Response({'msg': 'Login Success', **auth_data}, status=status.HTTP_200_OK)
        return Response({'msg': 'Invalid Credentials'}, status=status.HTTP_401_UNAUTHORIZED)


class LogoutView(APIView):
    """
    post:
    User log out
    """

    def post(self, request):
        """
        User log out.
        """
        logout(request)
        return Response({'msg': 'Successfully Logged out'}, status=status.HTTP_200_OK)


class HelloView(APIView):
    """
    get:
    Test view 'Hello, world!'
    """

    def get(self, request):
        """
        Test endpoint 'Hello world!'
        """
        content = {'message': 'Hello, World!'}
        return Response(content)


class CRUDProductView(APIView):
    """
    Product CRUD.

    get:
    Get product information.

    post:
    Create a new product. Only user with role 'seller' can create a new product.



    """
    permission_classes = (IsAuthenticated, SellerOnly)

    def get_permissions(self):
        if self.request.method in ['POST', 'DELETE', 'PUT']:
            return [permission() for permission in self.permission_classes]
        return [AllowAny()]

    test_param = openapi.Parameter(
        'product_id', openapi.IN_QUERY, description="Product ID", type=openapi.TYPE_INTEGER)

    @swagger_auto_schema(manual_parameters=[test_param])
    def get(self, request):
        """
        Get a product by product ID.
        """
        product_id: int = request.query_params.get('product_id')
        if not product_id:
            Response(status=status.HTTP_404_NOT_FOUND)
        product = get_object_or_404(Product, pk=product_id)
        product_serializer = ProductSerializer(product)
        return Response(product_serializer.data)

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'cost': openapi.Schema(type=openapi.TYPE_INTEGER),
            'amountAvailable': openapi.Schema(type=openapi.TYPE_INTEGER),
            'productName': openapi.Schema(type=openapi.TYPE_STRING),
        }
    ))
    def post(self, request):
        """
        Create a new product.
        """
        data: dict = request.data
        data['seller'] = request.user.id
        serializer = ProductSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(manual_parameters=[test_param])
    def delete(self, request):
        """
        Delete a product by ID.
        """
        product_id: int = request.query_params.get('product_id')
        if not product_id:
            Response(status=status.HTTP_404_NOT_FOUND)
        product = get_object_or_404(Product, pk=product_id)
        if request.user.id != product.seller_id:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        product.delete()
        return Response({'message': 'Product was deleted successfully!'},
                        status=status.HTTP_204_NO_CONTENT)

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'id': openapi.Schema(type=openapi.TYPE_INTEGER),
            'cost': openapi.Schema(type=openapi.TYPE_INTEGER),
            'amountAvailable': openapi.Schema(type=openapi.TYPE_INTEGER),
            'productName': openapi.Schema(type=openapi.TYPE_STRING),
        }
    ))
    def put(self, request):
        """
        Modify product. Only user seller of product can modify it.
        """
        data: dict = request.data
        product_id: int = data.get('id')
        user_id: int = request.user.id
        product = get_object_or_404(Product, pk=product_id)
        if user_id != product.seller_id:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        data['seller'] = user_id
        product_serializer = ProductSerializer(instance=product, data=data)
        if product_serializer.is_valid():
            product_serializer.save()
            return Response(product_serializer.data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class DepositView(APIView):
    permission_classes = (IsAuthenticated, BuyerOnly)

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'coin': openapi.Schema(type=openapi.TYPE_INTEGER)
        }
    ))
    def post(self, request):
        """
        Add a money to user deposit.
        """
        coin: int = request.data.get('coin')
        if not coin:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        user = request.user
        user_data = {'deposit': user.deposit + coin}
        serializer = UserSerializer(
            instance=user, data=user_data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ResetDepositView(APIView):
    permission_classes = (IsAuthenticated, BuyerOnly)

    def delete(self, request):
        """
        Reset user's deposit.
        """
        user = request.user
        user.reset_deposit()
        user.save()
        return Response(status=status.HTTP_201_CREATED)


class BuyProductView(APIView):
    permission_classes = (IsAuthenticated, BuyerOnly)

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'product_id': openapi.Schema(type=openapi.TYPE_INTEGER)
        }
    ))
    def post(self, request):
        """
        Buy product by product ID.
        """
        product_id: int = request.data.get('product_id')
        if not product_id:
            return Response(status=status.HTTP_404_NOT_FOUND)
        user: MyUser = request.user
        product: Product = get_object_or_404(Product, pk=product_id)
        if not product.is_available():
            return Response({'msg': 'Product is not available.'},
                            status=status.HTTP_400_BAD_REQUEST)
        if user.deposit < product.cost:
            return Response({'msg': 'User doesn\'t have enough deposit.'},
                            status=status.HTTP_400_BAD_REQUEST)
        change: list[int] = user.get_change(cost=product.cost)
        product.amountAvailable -= 1
        user.reset_deposit()
        user.save()
        product.save()
        return Response({'change': change}, status=status.HTTP_201_CREATED)
